## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `metrics-client` directory.
    3. Run `yarn/npm install` command to download and install all dependencies.
    4. To run this project use `yarn/npm start:dev` command in command line.
    5. To build the project for production run `yarn/npm build` command. This will build the app for production and put all the files in `/build` folder.
    6. To run production ready version of the app run `yarn start` command and go to `http://localhost:8080`.

## Table of contents from [wiki page](https://gitlab.com/zsaleem/metrics-client/-/wikis/Home)

1. [Home](https://gitlab.com/zsaleem/metrics-client/-/wikis/Home)
2. [Getting Started](https://gitlab.com/zsaleem/metrics-client/-/wikis/Getting-Started)
3. [Git Work Flow](https://gitlab.com/zsaleem/metrics-client/-/wikis/Git-Work-Flow)
4. [Development Environment](https://gitlab.com/zsaleem/metrics-client/-/wikis/Development-Environment)
5. [Architecture](https://gitlab.com/zsaleem/metrics-client/-/wikis/Architecture)
6. [DevOps](https://gitlab.com/zsaleem/metrics-client/-/wikis/DevOps)

## Live Demo
To view live demo please [click here](https://metric-client.herokuapp.com/).

To view list of CI/CD pipeline [click here](https://gitlab.com/zsaleem/metrics-client/-/pipelines).

To view list of all commits [click here](https://gitlab.com/zsaleem/metrics-client/-/commits/master).

To view list of all branches [click here](https://gitlab.com/zsaleem/metrics-client/-/branches/all).

To view list of all merge request [click here](https://gitlab.com/zsaleem/metrics-client/-/merge_requests?scope=all&state=all).

To view git branch strategy [click here](https://swimlanes.io/u/efcx3LmCi).

To view architecture diagram [click here](https://app.terrastruct.com/diagrams/1456080710#layer=2100111249).

#### Note 1: When first time the UI is loaded in browser, it takes quite a bit of time to load all the data from the AtlasDB database which the backend cache all the data and on the rest of the UI loads the performance will be good as the data will be loaded from cache in backend.

#### Note 2: Run the `metrics-client` project before running this `metrics-service` project for them work successfully.

#### Note 3: I added mock data between `28 Nov - 6 Dec`. Therefore, when testing please choose date between these dates.

#### Note 4: The line chart/graph in UI might seem like a staright line that is because the values I added to the database and the average that is calculated is usually between 4 and 6 therefore, it seems like staright line but it is not if you look at it a bit closely. Its just the average value is does not fluctuate a lot.