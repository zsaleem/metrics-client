import Container from '@mui/material/Container';
import { Home } from './pages/Home';

function App() {
  return (
    <Container maxWidth="lg">
      <Home />
    </Container>
  );
}

export default App;
