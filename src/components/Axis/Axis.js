import { useEffect, useRef } from 'react';
import { select, selectAll } from 'd3-selection';
import { axisBottom, axisLeft } from 'd3-axis';
import { transition } from 'd3-transition';

export const Axis = (props) => {
  const gRef = useRef();

  useEffect(() => {
    renderAxis();
  }, []);

  const renderAxis = () => {
    const { scale, orient, ticks } = props;
    const node = gRef.current;
    let axis;

    if (orient === 'bottom') {
      axis = axisBottom(scale);
    }
    if (orient === 'left') {
      axis = axisLeft(scale)
        .ticks(ticks);
    }
    select(node).call(axis);
  }

  const updateAxis = () => {
    const { scale, orient, ticks } = props;
    const t = transition().duration(1000)

    if (orient === 'left') {
      const axis = axisLeft(scale).ticks(ticks); 
      selectAll(`.${orient}`).transition(t).call(axis)
    }
  }
  
  const { orient, transform } = props;

  return (
    <g
      ref={gRef}
      transform={transform}
      className={`${orient} axis`}
    />
  );
};
