import { StyledLoader } from './Preloader.styled';

export const Preloader = () => {
	return (
		<StyledLoader />
	);
};
