import React, { useEffect, useRef } from 'react';
import { select, selectAll } from 'd3-selection';
import { transition } from 'd3-transition';

export const Line = (props) => {
  const divRef = useRef();

  useEffect(() => {
    const node = divRef.current;
    const {
    	xScale,
    	yScale,
    	data,
    	lineGenerator,
    } = props;
    const initialData = data.map(d => ({
      name: d.name,
      value: 0
    }));

    select(node)
      .append('path')
      .datum(initialData)
      .attr('id', 'line')
      .attr('stroke', 'black')
      .attr('stroke-width', 2)
      .attr('fill', 'none')
      .attr('d', lineGenerator);

    updateChart();
  }, []);

  const updateChart = () => {
    const {
      lineGenerator, xScale, yScale, data,
    } = props;

    const t = transition().duration(1000);

    const line = select('#line');

    line
      .datum(data)
      .transition(t)
      .attr('d', lineGenerator);
  };

  
  return <g className="line-group" ref={divRef} />;
};
