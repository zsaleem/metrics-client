import { useEffect, useState } from 'react';
import * as d3 from 'd3';
import { scaleLinear, scaleBand } from 'd3-scale';
import { line, curveMonotoneX } from 'd3-shape';
import { extent } from 'd3-array';
import { transition } from 'd3-transition';

import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import TextField from '@mui/material/TextField';
import DatePicker from '@mui/lab/DatePicker';
import TimePicker from '@mui/lab/TimePicker';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Card from '@mui/material/Card';
import moment from 'moment';

import { XYAxis } from '../../components/XY-Axis';
import { Line } from '../../components/Line';
import { Preloader } from '../../components/Preloader';

import { useFetcher } from '../../hooks/useFetcher';

import { StyledFilterContainer, useStyles } from './Home.styled';

export const Home = () => {
	const {
		data,
		setData,
		loader,
		setParams,
		params,
		setHttpProps,
  } = useFetcher();
	const margins = {
		top: 20,
		right: 20,
		bottom: 20,
		left: 20,
	};
	const { marginRight, marginTop, padding, } = useStyles();
	const parentWidth = 500;
  const width = parentWidth - margins.left - margins.right;
  const height = 200 - margins.top - margins.bottom;
  const ticks = 5;
  const t = transition().duration(1000);

  const onDivisorChange = (event, newDivisor) => {
  	setData([]);
    setParams((prev) => ({
    	...prev,
    	divisor: newDivisor,
    }));
  };

  useEffect(() => {
		setParams((prev) => ({
    	...prev,
    	date: moment(new Date()),
			time: moment(new Date()),
			hour: moment().hours(),
			minute: moment().minutes(),
			divisor: 'day',
    }));
  }, []);

  useEffect(() => {
  	setHttpProps(() => ({
    	method: 'GET',
    	endPoint: 'metrics',
    }));
  }, []);

  const generateFilterHeader = () => {
  	return (
  		<StyledFilterContainer>
	  		<LocalizationProvider dateAdapter={AdapterDateFns}>
	        <DatePicker
	          label="Date"
	          openTo="year"
	          views={['day']}
	          value={params?.date || moment(new Date())}
	          onChange={(newValue) => {
	          	setData([]);
	            setParams((prev) => ({
	            	...prev,
	            	date: moment(newValue),
	            }));
	            setHttpProps(() => ({
					    	method: 'GET',
					    	endPoint: 'metrics',
					    }));
	          }}
	          renderInput={(params) => <TextField {...params} />}
	          InputProps={{ className: marginRight }}
	        />
	        <TimePicker
	        	label="Time"
	          value={params?.time || moment(new Date())}
	          onChange={(newTime) => {
	          	setData([]);
	          	setParams((prev) => ({
	          		...prev,
	          		time: moment(newTime),
	          		hour: moment(newTime).hours(),
	          		minute: moment(newTime).minutes(),
	          	}));
							setHttpProps(() => ({
					    	method: 'GET',
					    	endPoint: 'metrics',
					    }));
	          }}
	          renderInput={(params) => <TextField {...params} />}
	          InputProps={{ className: marginRight }}
	        />
	      </LocalizationProvider>
	        <ToggleButtonGroup
			      color="primary"
			      value={params?.divisor || 'day'}
			      exclusive
			      onChange={onDivisorChange}
			      className={marginTop}
			    >
		      <ToggleButton value="day">Day</ToggleButton>
		      <ToggleButton value="hour">Hour</ToggleButton>
		      <ToggleButton value="minute">Minute</ToggleButton>
	    	</ToggleButtonGroup>
    	</StyledFilterContainer>
  	);
  };

  if (data.error) {
  	return (
  		<div>
  			{generateFilterHeader()}
  			<p>{data.message}</p>
  		</div>
  	);
  }

	const xScale = scaleBand()
    .domain(data.map(d => d.name))
    .rangeRound([0, width]).padding(0.1);

  const yScale = scaleLinear()
    .domain(extent([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], d => d))
    .range([height, 0])
    .nice();

  const lineGenerator = line()
    .x(d => xScale(d.name))
    .y(d => yScale(d.value))
    .curve(curveMonotoneX);

  return (
    <div>
    	{generateFilterHeader()}
    	{
    		loader
    		?
    			<Preloader />
    		:
	    		data.length > 0 ? (
	    			<Card
	    				className={padding}
	    				sx={{ maxWidth: width + margins.left + margins.right }}>
			        <svg
			          className="lineChartSvg"
			          width={width + margins.left + margins.right}
			          height={height + margins.top + margins.bottom}
			        >
			          <g transform={`translate(${margins.left}, ${margins.top})`}>
			            <XYAxis
			            	{...{ xScale, yScale, height, ticks, t }}
			            />
			            <Line
			            	data={data}
			            	xScale={xScale}
			            	yScale={yScale}
			            	lineGenerator={lineGenerator}
			            	width={width}
			            	height={height}
			            />
			          </g>
			        </svg>
		        </Card>
	    		) : (
	    			<p>No data available for the selected date.</p>
	    		)
    	}
    </div>
  );
};