import DatePicker from '@mui/lab/DatePicker';
import { styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const StyledFilterContainer = styled('div')({
	marginTop: '10px',
	marginBottom: '50px',
});

export const useStyles = makeStyles({
	marginRight: {
		marginRight: '30px',
	},
	marginTop: {
		marginTop: '3px',
	},
	padding: {
		padding: '1rem',
	}
});
