import { useEffect, useState } from 'react';
import moment from 'moment';
import axios from 'axios';

const generateURL = ({ method, endPoint }, ...rest) => {
	const baseURL = process.env.REACT_APP_API_ENDPOINT;
	let params = '', index = 1;

	const paramsCriteria = {
		1: (key, value) => `?${key}=${value}`,
		2: (key, value) => `&${key}=${value}`,
	};

	for (const [key, value] of Object.entries(rest[0])) {
		params = `${params}${paramsCriteria[index](key, value)}`;
	  index = 2;
	}

	const url = (method === 'GET') &&
								`${baseURL}${endPoint}${params}` ||
								`${baseURL}${endPoint}`;

	return url;
}

export const useFetcher = () => {
	const [data, setData] = useState([]);
	const [loader, setLoader] = useState(false);
	const [httpProps, setHttpProps] = useState({});
	const [params, setParams] = useState({});

	useEffect(() => {
		async function load() {
			setLoader(true);
			const response = await axios({
			  method: httpProps?.method,
			  url: generateURL(httpProps, { ...params }),
			  ...(httpProps?.method !== 'GET' && { data: { ...params } }),
			});
			
			setData(response.data);
			setLoader(false);
		}

		load();

		return () => undefined;
	}, [JSON.stringify(httpProps), JSON.stringify(params)]);

	return {
		data,
		setData,
		loader,
		setParams,
		params,
		setHttpProps,
	}
};
